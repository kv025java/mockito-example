package soft.serve;

import org.junit.*;

public class CalculatorTest {

    Calculator calculator;
    int a = 2;
    int b = 2;

    @Before
    public void setup() {
        System.out.println("setting up calculator");
        calculator = new Calculator();
    }

    @After
    public void tearDown() {
        System.out.println("setting null");
        calculator = null;
    }

    @BeforeClass
    public static void setupclass() {
        System.out.println("setting up before class");
    }

    @Test
    public void addWorksTest() {
        // Arrange
        var expectedResult = 4;
        // Act
        var result = calculator.add(a, b);
        // Assert
        Assert.assertEquals("2 + 2 not equals what ever", expectedResult, result);
    }

    @Test
    public void addWorks2Test() {
        // Arrange
        var expectedResult = 3;
        var c = 1;
        // Act
        var result = calculator.add(a, c);
        // Assert
        Assert.assertEquals("2 + 1 not equals what ever", expectedResult, result);
    }
}
