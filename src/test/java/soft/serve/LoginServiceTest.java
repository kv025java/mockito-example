package soft.serve;

import org.junit.Test;
import static org.mockito.Mockito.*;

public class LoginServiceTest {
    @Test
    public void testItShouldSetAccountToLoggedInWhenPasswordMatches() {
        IAccount account = mock(IAccount.class);
        when(account.passwordMatches(anyString())).thenReturn(true);

        IAccountRepository accountRepository = mock(IAccountRepository.class);
        when(accountRepository.find(anyString())).thenReturn(account);

        LoginService service = new LoginService(accountRepository);

        service.login("admin", "password");

        verify(account, times(1)).setLoggedIn(true);
    }

    @Test
    public void testItShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
        IAccount account = mock(IAccount.class);
        when(account.passwordMatches(anyString())).thenReturn(false);

        IAccountRepository accountRepository = mock(IAccountRepository.class);
        when(accountRepository.find(anyString())).thenReturn(account);

        LoginService service = new LoginService(accountRepository);
        for (int i = 0; i < 3; ++i)
            service.login("admin", "password");

        verify(account, times(1)).setRevoked(true);
    }

    @Test
    public void testItShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
        IAccount account = mock(IAccount.class);
        when(account.passwordMatches(anyString())).thenReturn(false);

        IAccountRepository accountRepository = mock(IAccountRepository.class);
        when(accountRepository.find(anyString())).thenReturn(account);

        LoginService service = new LoginService(accountRepository);

        service.login("admin", "password");
        verify(account, never()).setLoggedIn(true);
    }


}
