package soft.serve;

public interface IAccountRepository {
    IAccount find(String accountId);
}

