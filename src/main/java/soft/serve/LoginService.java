package soft.serve;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class LoginService {
    private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

    private final IAccountRepository accountRepository;
    private int failedAttempts = 0;

    LoginService(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
        logger.info("Instantiated the repository: {}", accountRepository);
    }
    void login(String accountId, String password) {
        logger.info("Login started: User: {}, password: {}", accountId, password);

        IAccount account = accountRepository.find(accountId);
        logger.info("Got account from repo: {}", account);

        if (account.passwordMatches(password)) {
            account.setLoggedIn(true);
            logger.info("Login success");
        }
        else {
            ++failedAttempts;
            logger.info("{} failed attemps", failedAttempts);
        }


        if (failedAttempts == 3) {
            account.setRevoked(true);
            logger.info("Account revoked");
        }


    }
}


