package soft.serve;

public interface IAccount {
    void setLoggedIn(boolean value);
    boolean passwordMatches(String candidate);
    void setRevoked(boolean value);
}

